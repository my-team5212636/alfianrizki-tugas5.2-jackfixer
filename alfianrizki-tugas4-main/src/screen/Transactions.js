import {StyleSheet, Text, View, TouchableOpacity, FlatList} from 'react-native';
import React from 'react';
import {useSelector} from 'react-redux';

export default function Transactions({navigation}) {
  const {transaction} = useSelector(state => state.transaction);
  console.log('transaction: ', transaction);

  const navigate = ({item}) => {
    navigation.navigate('TransactionNavigation', {
      params: {item},
      screen: 'DetailTransaction',
    });

    console.log('item: ', item);
  };

  return (
    <View style={styles.screen}>
      <FlatList
        data={transaction}
        keyExtractor={index => index.toString()}
        renderItem={({item}) => (
          <TouchableOpacity
            style={styles.item}
            onPress={() => navigate({item})}>
            <View style={{flexDirection: 'row', marginBottom: 13}}>
              <Text>{item.date}</Text>
              {/* <Text style={{marginLeft: 10}}>09:00</Text> */}
            </View>
            <View style={{flexDirection: 'row', marginBottom: 5}}>
              <Text style={{color: 'black', fontWeight: '400'}}>
                {item.merk} -
              </Text>
              <Text style={{color: 'black', fontWeight: '400'}}>
                {' '}
                {item.warna} -
              </Text>
              <Text style={{color: 'black', fontWeight: '400'}}>
                {' '}
                {item.ukuran}
              </Text>
            </View>
            <Text
              style={{
                color: 'black',
                fontSize: 15,
                fontWeight: '300',
                marginBottom: 13,
              }}>
              Cuci Sepatu
            </Text>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View style={{flexDirection: 'row'}}>
                <Text style={{color: 'black'}}>Kode Reservasi : </Text>
                <Text style={{color: 'black', fontWeight: 'bold'}}>
                  {item.kode}
                </Text>
              </View>
              <View
                style={{
                  backgroundColor: '#F29C1F29',
                  borderRadius: 20,
                  paddingHorizontal: 10,
                  paddingVertical: 3,
                }}>
                <Text style={{color: '#FFC107'}}>Reserved</Text>
              </View>
            </View>
          </TouchableOpacity>
        )}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: '#F6F8FF',
    padding: 15,
  },
  item: {
    backgroundColor: 'white',
    paddingHorizontal: 12,
    paddingVertical: 18,
    borderRadius: 10,
    marginBottom: 10,
  },
});
