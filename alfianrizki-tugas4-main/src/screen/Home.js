import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput,
  ScrollView,
} from 'react-native';
import React from 'react';

const Home = ({navigation}) => {
  return (
    <View style={{flex: 1}}>
      <ScrollView>
        <View style={styles.topMenu}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Image source={require('../assets/image/image_profile.png')} />
            <TouchableOpacity style={{justifyContent: 'center'}}>
              <Image source={require('../assets/icon/ic_bag.png')} />
            </TouchableOpacity>
          </View>
          <Text style={styles.profileText}>Hello, Agil!</Text>
          <Text style={styles.title}>
            Ingin merawat dan perbaiki{'\n'}sepatumu? cari disini!
          </Text>
          <View style={{flexDirection: 'row'}}>
            <View style={styles.inputSearch}>
              <Image
                source={require('../assets/icon/ic_search.png')}
                style={{marginRight: 10}}
              />
              <TextInput style={{width: '85%'}} />
            </View>
            <View style={styles.filter}>
              <Image source={require('../assets/icon/ic_filter.png')} />
            </View>
          </View>
        </View>
        <View style={{backgroundColor: '#F6F8FF', padding: 20}}>
          <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <TouchableOpacity style={styles.menuItem}>
              <Image
                source={require('../assets/image/image_shoe.png')}
                style={{marginBottom: 5}}
              />
              <Text style={styles.menuText}>Sepatu</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.menuItem}>
              <Image
                source={require('../assets/image/image_jacket.png')}
                style={{marginBottom: 5}}
              />
              <Text style={styles.menuText}>Jaket</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.menuItem}>
              <Image
                source={require('../assets/image/image_bag.png')}
                style={{marginBottom: 5}}
              />
              <Text style={styles.menuText}>Tas</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginBottom: 25,
            }}>
            <Text style={{fontWeight: 'bold', fontSize: 15, color: 'black'}}>
              Rekomendasi Terdekat
            </Text>
            <TouchableOpacity>
              <Text style={{color: '#E64C3C'}}>View All</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity style={styles.menuPlace}>
            <Image
              source={require('../assets/image/image_place1.png')}
              style={{marginRight: 15, alignSelf: 'center'}}
            />
            <View style={{flex: 1}}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginBottom: 5,
                  marginTop: 10,
                }}>
                <View>
                  <Image source={require('../assets/image/image_rating.png')} />
                  <Text style={{fontSize: 10}}>4.8 Ratings</Text>
                </View>
                <Image source={require('../assets/icon/ic_redhart.png')} />
              </View>
              <Text
                style={{
                  color: 'black',
                  fontSize: 15,
                  fontWeight: 'bold',
                  marginBottom: 5,
                }}>
                Jack Repair Gejayan
              </Text>
              <Text style={{fontSize: 10, marginBottom: 10}}>
                Jl. Gejayan III No.2, Karangasem, Kec. Laweyan . . .
              </Text>
              <View
                style={{
                  backgroundColor: '#E64C3C33',
                  width: 60,
                  padding: 5,
                  borderRadius: 20,
                  alignItems: 'center',
                }}>
                <Text
                  style={{fontWeight: 'bold', fontSize: 12, color: '#EA3D3D'}}>
                  TUTUP
                </Text>
              </View>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.menuPlace}
            onPress={() =>
              navigation.navigate('HomeNavigation', {screen: 'Detail'})
            }>
            <Image
              source={require('../assets/image/image_place2.png')}
              style={{marginRight: 15, alignSelf: 'center'}}
            />
            <View style={{flex: 1}}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginBottom: 5,
                  marginTop: 10,
                }}>
                <View>
                  <Image source={require('../assets/image/image_rating.png')} />
                  <Text style={{fontSize: 10}}>4.7 Ratings</Text>
                </View>
                <Image source={require('../assets/icon/ic_heart.png')} />
              </View>
              <Text
                style={{
                  color: 'black',
                  fontSize: 15,
                  fontWeight: 'bold',
                  marginBottom: 5,
                }}>
                Jack Repair Seturan
              </Text>
              <Text style={{fontSize: 10, marginBottom: 10}}>
                Jl. Seturan Kec. Laweyan . . ..
              </Text>
              <View
                style={{
                  backgroundColor: '#11A84E1F',
                  width: 60,
                  padding: 5,
                  borderRadius: 20,
                  alignItems: 'center',
                }}>
                <Text
                  style={{fontWeight: 'bold', fontSize: 12, color: '#11A84E'}}>
                  BUKA
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  topMenu: {
    paddingHorizontal: 25,
    paddingTop: 55,
    paddingBottom: 28,
    backgroundColor: 'white',
  },
  profileText: {
    marginTop: 5,
    color: '#034262',
    fontSize: 20,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 25,
    color: 'black',
    marginBottom: 20,
  },
  inputSearch: {
    flexDirection: 'row',
    backgroundColor: '#F6F8FF',
    paddingLeft: 10,
    borderRadius: 10,
    width: 300,
    marginRight: 15,
    alignItems: 'center',
  },
  filter: {
    backgroundColor: '#F6F8FF',
    padding: 15,
    borderRadius: 10,
  },
  menuItem: {
    backgroundColor: 'white',
    paddingHorizontal: 25,
    paddingVertical: 15,
    borderRadius: 15,
    marginHorizontal: 50,
    marginBottom: 25,
  },
  menuText: {
    color: '#BB2427',
  },
  menuPlace: {
    flexDirection: 'row',
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 10,
    marginBottom: 5,
  },
});

export default Home;
