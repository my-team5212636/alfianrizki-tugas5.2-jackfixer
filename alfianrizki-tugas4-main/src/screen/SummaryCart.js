import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import React, {useState} from 'react';
import ModalShowSuccess from './ModalShowSuccess';
import {useDispatch, useSelector} from 'react-redux';

export default function SummaryCart({route, navigation}) {
  const {merk, warna, ukuran} = route.params;
  const [showModal, setShowModal] = useState(false);

  const nama = 'Agil Bani';
  const telp = '0813763476';
  const alamat = 'Jl. Perumnas, Condong catur, Sleman, Yogyakarta';
  const toko = 'Jack Repair - Seturan';

  const {transaction} = useSelector(state => state.transaction);

  const dispatch = useDispatch();

  const addData = () => {
    var date = new Date();

    var dataTransaction = [...transaction];
    const data = {
      kode: date.getMilliseconds(),
      nama: nama,
      telp: telp,
      alamat: alamat,
      toko: toko,
      date: date.toString(),
      merk: merk,
      warna: warna,
      ukuran: ukuran,
    };
    dataTransaction.push(data);
    dispatch({type: 'ADD_DATA', data: dataTransaction});
    setShowModal(true);
  };

  return (
    <View style={styles.screen}>
      <ModalShowSuccess
        show={showModal}
        onClose={() => navigation.navigate('Home')}
        onClick={() =>
          navigation.navigate('TransactionNavigation', {
            screen: 'DetailTransaction',
          })
        }
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.segment}>
          <Text style={styles.segmentTitle}>Data Customer</Text>
          <View style={{flexDirection: 'row'}}>
            <Text style={styles.text}>{nama}</Text>
            <Text
              style={{
                fontSize: 18,
                marginBottom: 10,
                color: 'black',
                marginLeft: 10,
                fontWeight: '300',
              }}>
              ({telp})
            </Text>
          </View>
          <Text style={styles.text}>{alamat}</Text>
          <Text style={styles.text}>gantengdoang@dipanggang.com</Text>
        </View>
        <View style={styles.segment}>
          <Text style={styles.segmentTitle}>Alamat Outlet Tujuan</Text>
          <View style={{flexDirection: 'row'}}>
            <Text style={styles.text}>{toko}</Text>
            <Text
              style={{
                fontSize: 18,
                marginBottom: 10,
                color: 'black',
                marginLeft: 10,
                fontWeight: '300',
              }}>
              (027-343457)
            </Text>
          </View>
          <Text style={styles.text}>Jl. Affandi No 18, Sleman, Yogyakarta</Text>
        </View>
        <View style={styles.segmentBarang}>
          <Image source={require('../assets/image/img_shoes_cart.png')} />
          <View style={{marginLeft: 13}}>
            <View style={{flexDirection: 'row', marginBottom: 10}}>
              <Text style={{color: 'black', fontWeight: '400'}}>{merk} -</Text>
              <Text style={{color: 'black', fontWeight: '400'}}>
                {' '}
                {warna} -
              </Text>
              <Text style={{color: 'black', fontWeight: '400'}}> {ukuran}</Text>
            </View>
            <Text style={{color: '#737373', marginBottom: 10}}>
              Cuci Sepatu
            </Text>
            <Text style={{color: '#737373'}}>Note : -</Text>
          </View>
        </View>
      </ScrollView>
      <TouchableOpacity style={styles.button} onPress={() => addData()}>
        <Text style={styles.txtButton}>Reservasi Sekarang</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: '#F6F8FF',
    paddingTop: 5,
    paddingBottom: 50,
  },
  segment: {
    backgroundColor: 'white',
    paddingHorizontal: 25,
    paddingVertical: 20,
    marginBottom: 12,
    width: '100%',
  },
  segmentTitle: {
    color: '#979797',
    fontSize: 18,
    marginBottom: 10,
  },
  text: {
    fontSize: 18,
    marginBottom: 10,
    color: 'black',
    fontWeight: '300',
  },
  segmentBarang: {
    backgroundColor: 'white',
    paddingHorizontal: 25,
    paddingVertical: 20,
    marginBottom: 50,
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
  },
  button: {
    backgroundColor: '#BB2427',
    borderRadius: 10,
    paddingVertical: 15,
    alignItems: 'center',
    position: 'absolute',
    bottom: 1,
    width: '100%',
    marginBottom: 20,
    width: '90%',
    alignSelf: 'center',
  },
  txtButton: {
    color: 'white',
    fontSize: 15,
    fontWeight: '500',
  },
});
