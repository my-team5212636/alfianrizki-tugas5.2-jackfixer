import {StyleSheet, Text, View, Image} from 'react-native';
import React, {useEffect} from 'react';

export default function SplashScreen({navigation, route}) {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('AuthNavigation');
    }, 1000);
  }, []);
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Image
        source={require('../assets/image/splashImage.png')}
        style={{width: 150, height: 150, resizeMode: 'contain'}}
      />
    </View>
  );
}

const styles = StyleSheet.create({});
