import {
  StyleSheet,
  Text,
  View,
  TextInput,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import React, {useState} from 'react';
import CheckBox from '@react-native-community/checkbox';
export default function Form({navigation}) {
  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  const [merk, setMerek] = useState('');
  const [warna, setWarna] = useState('');
  const [ukuran, setUkuran] = useState('');
  return (
    <View style={styles.screen}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Text style={styles.text}>Merek</Text>
        <TextInput
          onChangeText={text => setMerek(text)}
          style={styles.inputText}
          placeholder="Masukkan Merk barang"
        />
        <Text style={styles.text}>Warna</Text>
        <TextInput
          onChangeText={text => setWarna(text)}
          style={styles.inputText}
          placeholder="Warna Barang, cth : Merah - Putih"
        />
        <Text style={styles.text}>Ukuran</Text>
        <TextInput
          onChangeText={text => setUkuran(text)}
          style={styles.inputText}
          placeholder="Cth : S, M, L / 39,40"
        />
        <Text style={styles.text}>Photo</Text>
        <TouchableOpacity style={styles.inputPhoto}>
          <Image source={require('../assets/icon/ic_camera_red.png')} />
          <Text style={{color: '#BB2427', marginTop: 5}}>Add Photo</Text>
        </TouchableOpacity>
        <View style={styles.checkboxWrapper}>
          <CheckBox
            disabled={false}
            value={toggleCheckBox}
            onValueChange={newValue => setToggleCheckBox(newValue)}
            style={{marginRight: 10}}
          />
          <Text style={{color: 'black', fontWeight: '300'}}>
            Ganti Sol Sepatu
          </Text>
        </View>
        <View style={styles.checkboxWrapper}>
          <CheckBox
            disabled={false}
            value={toggleCheckBox}
            onValueChange={newValue => setToggleCheckBox(newValue)}
            style={{marginRight: 10}}
          />
          <Text style={{color: 'black', fontWeight: '300'}}>Jahit Sepatu</Text>
        </View>
        <View style={styles.checkboxWrapper}>
          <CheckBox
            disabled={false}
            value={toggleCheckBox}
            onValueChange={newValue => setToggleCheckBox(newValue)}
            style={{marginRight: 10}}
          />
          <Text style={{color: 'black', fontWeight: '300'}}>
            Repaint Sepatu
          </Text>
        </View>
        <View style={styles.checkboxWrapper}>
          <CheckBox
            disabled={false}
            value={toggleCheckBox}
            onValueChange={newValue => setToggleCheckBox(newValue)}
            style={{marginRight: 10}}
          />
          <Text style={{color: 'black', fontWeight: '300'}}>Cuci Sepatu</Text>
        </View>
        <Text
          style={{
            color: '#BB2427',
            fontSize: 15,
            fontWeight: '500',
            marginBottom: 10,
            marginTop: 20,
          }}>
          Catatan
        </Text>
        <TextInput
          placeholder="Cth : ingin ganti sol baru"
          multiline={true}
          numberOfLines={4}
          textAlignVertical="top"
          style={{
            backgroundColor: '#F6F8FF',
            width: '100%',
            borderRadius: 10,
            padding: 10,
            marginBottom: 40,
          }}
        />
        <TouchableOpacity
          style={styles.button}
          onPress={() =>
            navigation.navigate('Cart', {
              merk: merk,
              ukuran: warna,
              warna: ukuran,
            })
          }>
          <Text style={styles.txtButton}>Masukkan Keranjang</Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    paddingHorizontal: 20,
    paddingVertical: 25,
    backgroundColor: 'white',
  },
  text: {
    color: '#BB2427',
    fontSize: 15,
    fontWeight: '500',
    marginBottom: 10,
  },
  inputText: {
    backgroundColor: '#F6F8FF',
    width: '100%',
    borderRadius: 10,
    paddingHorizontal: 10,
    marginBottom: 25,
  },
  inputPhoto: {
    backgroundColor: 'white',
    paddingHorizontal: 10,
    paddingVertical: 20,
    alignItems: 'center',
    borderRadius: 10,
    borderColor: '#BB2427',
    borderWidth: 1,
    width: '25%',
    marginBottom: 40,
  },
  checkboxWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 15,
  },
  button: {
    backgroundColor: '#BB2427',
    borderRadius: 10,
    paddingVertical: 15,
    alignItems: 'center',
  },
  txtButton: {
    color: 'white',
    fontSize: 20,
    fontWeight: '500',
  },
});
