import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import {useDispatch} from 'react-redux';

export default function DetailTransaction({navigation, route}) {
  const [kode, setKode] = useState('');
  const [nama, setNama] = useState('');
  const [telp, setTelp] = useState('');
  const [alamat, setAlamat] = useState('');
  const [toko, setToko] = useState('');
  const [date, setDate] = useState('');
  const [merk, setMerk] = useState('');
  const [warna, setWarna] = useState('');
  const [ukuran, setUkuran] = useState('');

  const dispatch = useDispatch();

  console.log('params', route.params);
  const checkData = () => {
    if (route.params) {
      const data = route.params.item;
      setKode(data.kode);
      setNama(data.nama);
      setTelp(data.telp);
      setAlamat(data.alamat);
      setToko(data.toko);
      setDate(data.date);
      setMerk(data.merk);
      setWarna(data.warna);
      setUkuran(data.ukuran);
    }
  };

  useEffect(() => {
    checkData();
  }, []);

  const deleteData = async () => {
    dispatch({type: 'DELETE_DATA', id: route.params.item.kode});
    navigation.goBack();
  };

  return (
    <View style={{flex: 1}}>
      <ScrollView>
        <View style={styles.topContent}>
          <View style={{flexDirection: 'row', marginBottom: 50}}>
            <Text style={{marginRight: 10, color: '#BDBDBD'}}>{date}</Text>
          </View>
          <Text
            style={{
              color: 'black',
              fontSize: 40,
              fontWeight: 'bold',
            }}>
            {kode}
          </Text>
          <Text
            style={{
              color: 'black',
              fontSize: 15,
              fontWeight: '400',
              marginBottom: 40,
            }}>
            Kode Reservasi
          </Text>
          <Text style={{color: '#6F6F6F', fontSize: 17, fontWeight: '300'}}>
            Sebutkan Kode Reservasi saat
          </Text>
          <Text style={{color: '#6F6F6F', fontSize: 17, fontWeight: '300'}}>
            tiba di outlet
          </Text>
        </View>
        <View style={styles.bottomContent}>
          <Text style={styles.title}>Barang</Text>
          <View style={styles.content}>
            <Image source={require('../assets/image/img_shoes_cart.png')} />
            <View style={{marginLeft: 13}}>
              <View style={{flexDirection: 'row', marginBottom: 10}}>
                <Text style={{color: 'black', fontWeight: '400'}}>
                  {merk} -
                </Text>
                <Text style={{color: 'black', fontWeight: '400'}}>
                  {' '}
                  {warna} -
                </Text>
                <Text style={{color: 'black', fontWeight: '400'}}>
                  {' '}
                  {ukuran}
                </Text>
              </View>
              <Text style={{color: '#737373', marginBottom: 10}}>
                Cuci Sepatu
              </Text>
              <Text style={{color: '#737373'}}>Note : -</Text>
            </View>
          </View>
          <Text style={styles.title}>Status Pesanan</Text>
          <TouchableOpacity
            style={styles.content}
            onPress={() =>
              navigation.navigate('TransactionNavigation', {screen: 'Checkout'})
            }>
            <Image
              source={require('../assets/icon/ic_circle_red.png')}
              style={{marginRight: 10}}
            />
            <View style={{flexDirection: 'row'}}>
              <View style={{width: '85%'}}>
                <Text
                  style={{
                    color: 'black',
                    fontSize: 17,
                    fontWeight: '300',
                  }}>
                  Telah Reservasi
                </Text>
                <Text style={{color: '#A5A5A5', fontSize: 13}}>
                  20 Desember 2020
                </Text>
              </View>
              <Text style={{color: '#A5A5A5', fontSize: 13}}>09:00</Text>
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  topContent: {
    width: '100%',
    backgroundColor: 'white',
    paddingVertical: 20,
    alignItems: 'center',
  },
  bottomContent: {
    backgroundColor: '#F6F8FF',
    paddingHorizontal: 11,
    paddingVertical: 18,
  },
  title: {
    color: 'black',
    fontWeight: '300',
    marginBottom: 15,
  },
  content: {
    backgroundColor: 'white',
    paddingHorizontal: 15,
    paddingVertical: 25,
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    marginBottom: 30,
  },
});
