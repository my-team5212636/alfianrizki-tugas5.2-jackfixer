import {View, Text} from 'react-native';
import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Home from './screen/Home';
import Detail from './screen/Detail';
import Form from './screen/Form';
import Cart from './screen/Cart';
import SummaryCart from './screen/SummaryCart';
import ModalShowSuccess from './screen/ModalShowSuccess';

const Stack = createNativeStackNavigator();

export default function HomeNavigation() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="HomePage" component={Home} />
      <Stack.Screen name="Detail" component={Detail} />
      <Stack.Screen name="Form" component={Form} />
      <Stack.Screen name="Cart" component={Cart} />
      <Stack.Screen name="Summary" component={SummaryCart} />
    </Stack.Navigator>
  );
}
