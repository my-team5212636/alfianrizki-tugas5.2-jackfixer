const initialState = {
  transaction: [],
};

const transactionReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_DATA':
      return {
        ...state,
        transaction: action.data,
      };

    case 'DELETE_DATA':
      var newData = [...state.transaction];
      var findIndex = state.transaction.findIndex(value => {
        return value.kode === action.kode;
      });
      newData.splice(findIndex, 1);
      return {
        ...state,
        transaction: newData,
      };
    default:
      return state;
  }
};

export default transactionReducer;
