import {combineReducers} from 'redux';
import transactionReducer from './reducers/transactionReducer';

const rootReducer = combineReducers({
  transaction: transactionReducer,
});

export default rootReducer;
